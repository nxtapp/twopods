//
//  NumberFormattingTest.swift
//  TwoPods
//
//  Created by Adriaan on 03/05/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest
@testable import TwoPods

class NumberFormattingTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIntFormatting() {
        let number: Int = 34
        XCTAssertEqual("0034", number.format("04"))
        XCTAssertEqual("  34", number.format("4"))
    }
    
    func testFloatFormatting() {
        let number: Float = Float(141.675365)
        
        XCTAssertEqual("142", number.format(".0"))
        XCTAssertEqual("141.7", number.format(".1"))
        XCTAssertEqual("141.68", number.format(".2"))
        
        XCTAssertEqual("141.675", number.format(".3"))
        XCTAssertEqual("141", Float(141.3).format(".0"))
        
        XCTAssertEqual("141.00", Float(141.0).format(".2"))
        XCTAssertEqual("141.10", Float(141.1).format(".2"))
    }
    
    func testCGFloatFormatting() {
        let number: CGFloat = CGFloat(41.675365)
        
        XCTAssertEqual("42", number.format(".0"))
        XCTAssertEqual("41.7", number.format(".1"))
        XCTAssertEqual("41.68", number.format(".2"))
        
        XCTAssertEqual("41.675", number.format(".3"))
        XCTAssertEqual("41", CGFloat(41.3).format(".0"))
        
        XCTAssertEqual("41.00", CGFloat(41.0).format(".2"))
    }
    
    func testDoubleFormatting() {
        let number: Double = Double(45.675365)
        
        XCTAssertEqual("46", number.format(".0"))
        XCTAssertEqual("45.7", number.format(".1"))
        XCTAssertEqual("45.68", number.format(".2"))
        
        XCTAssertEqual("45.675", number.format(".3"))
        XCTAssertEqual("45", Double(45.3).format(".0"))
        
        XCTAssertEqual("45.00", Double(45.0).format(".2"))
    }
}
