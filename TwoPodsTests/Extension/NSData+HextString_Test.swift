//
//  NSData+HextString_Test.swift
//  TwoPods
//
//  Created by Adriaan on 06/06/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class NSData_HexString_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDataToHexString() {
        let textData: NSData = "Hallo".dataUsingEncoding(NSUTF8StringEncoding)!
        let expectedHash = "48616c6c6f"
        
        XCTAssertEqual(expectedHash, textData.hexString)
    }
    
}
