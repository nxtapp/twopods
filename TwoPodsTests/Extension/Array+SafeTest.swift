//
//  Array+SafeTest.swift
//  TwoPods
//
//  Created by Adriaan on 28/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class ArraySafeTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSafeArray() {
        let array: Array = ["String1", "String2"]
        
        XCTAssertEqual("String1", array[safe: 0])
        XCTAssertEqual("String2", array[safe: 1])
        XCTAssertNil(array[safe: 2])
    }
}
