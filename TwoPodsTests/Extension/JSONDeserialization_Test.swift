//
//  JSONDeserialization_Test.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 01/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class JSONDeserializationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testJSONStringDeserialization() {
        let jsonString = "{\"key\":\"value\",\"array\":[\"Obj1\"],\"boolean\":true,\"number\":10,\"object\":{\"inner_key\":\"texted\"}}"
        guard let dict = jsonString.deserializedJSON else {
            XCTAssertTrue(false, "JSON not deserialized as String")
            return
        }
        
        XCTAssertEqual(dict["key"] as? String, "value")
        XCTAssertEqual(dict["array"]?[0] as? String, "Obj1")
        XCTAssertEqual(dict["boolean"] as? Bool, true)
        XCTAssertEqual(dict["number"] as? Int, 10)
        XCTAssertEqual((dict["object"] as? NSDictionary)?["inner_key"] as? String, "texted")
    }
}