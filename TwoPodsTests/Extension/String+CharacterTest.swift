//
//  String+CharacterTest.swift
//  TwoPods
//
//  Created by Adriaan on 12/05/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest
@testable import TwoPods

class StringCharacterTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetCharacterAtIndex() {
        
        let value = "abcdefghijklmnoqrstuvwxyz1234567890"
        
        XCTAssertEqual(value[4], "e")
        
        XCTAssertEqual(value[0], "a" as Character)
        
        XCTAssertEqual(value[0...2], "abc")
        XCTAssertEqual(value[0..<2], "ab")
        XCTAssertEqual(value[2..<4], "cd")
        
        XCTAssertEqual(value[0..<35], "abcdefghijklmnoqrstuvwxyz1234567890")
        XCTAssertEqual(value[0...34], "abcdefghijklmnoqrstuvwxyz1234567890")
        
        XCTAssertEqual(value[0...24], "abcdefghijklmnoqrstuvwxyz")
        XCTAssertEqual(value[25...34], "1234567890")
    }
}
