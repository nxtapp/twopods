//
//  UIApplication+Documents_Test.swift
//  TwoPods
//
//  Created by Adriaan on 06/06/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest
@testable import TwoPods

class UIApplication_Documents_Test: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testApplicationDocumentsFolder() {
        XCTAssertNotNil(UIApplication.applicationDocumentsDirectory())
    }
}