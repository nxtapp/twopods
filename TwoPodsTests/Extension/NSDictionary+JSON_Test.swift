//
//  NSDictionary+JSON_Test.swift
//  TwoPods
//
//  Created by Arjan Duijzer on 31/03/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import XCTest

@testable import TwoPods

class NSDictionaryJSONTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEmptyDictionary() {
        let dictionary = NSDictionary()
        let json: String = dictionary.JSONSerialized!
        #if DEBUG_SWIFT
        
            XCTAssertEqual(json, "{\n\n}")
        #else
            XCTAssertEqual(json, "{}")
        #endif
    }
    
}

