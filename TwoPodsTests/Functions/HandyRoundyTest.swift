//
// Created by Adriaan on 18/05/16.
// Copyright (c) 2016 TwoBulls. All rights reserved.
//

import XCTest
@testable import TwoPods

class HandyRoundyTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRounding() {
        XCTAssertEqual(round(52.376, toNearest: 0.01), 52.38)
//        XCTAssertEqual(round(52.376, toNearest: 0.1), 52.4)
        XCTAssertEqual(round(52.376, toNearest: 0.25), 52.5)
        XCTAssertEqual(round(52.376, toNearest: 0.5), 52.5)
        XCTAssertEqual(round(52.376, toNearest: 1), 52)
        XCTAssertEqual(round(52.376, toNearest: 10), 50)
        XCTAssertEqual(round(52.376, toNearest: 100), 100)
        XCTAssertEqual(round(52.376, toNearest: 1000), 0)
    }

    func testRoundingDown() {
//        XCTAssertEqual(roundDown(52.376, toNearest: 0.01), 52.37)
//        XCTAssertEqual(roundDown(52.376, toNearest: 0.1), 52.3)
        XCTAssertEqual(roundDown(52.376, toNearest: 0.25), 52.25)
        XCTAssertEqual(roundDown(52.376, toNearest: 0.5), 52)
        XCTAssertEqual(roundDown(52.376, toNearest: 1), 52)
        XCTAssertEqual(roundDown(52.376, toNearest: 10), 50)
        XCTAssertEqual(roundDown(52.376, toNearest: 100), 0)
        XCTAssertEqual(roundDown(52.376, toNearest: 1000), 0)
    }

    func testRoundingUp() {
        XCTAssertEqual(roundUp(52.376, toNearest: 0.01), 52.38)
//        XCTAssertEqual(roundUp(52.376, toNearest: 0.1), 52.4)
        XCTAssertEqual(roundUp(52.376, toNearest: 0.25), 52.5)
        XCTAssertEqual(roundUp(52.376, toNearest: 0.5), 52.5)
        XCTAssertEqual(roundUp(52.376, toNearest: 1),  53)
        XCTAssertEqual(roundUp(52.376, toNearest: 5), 55)
        XCTAssertEqual(roundUp(52.376, toNearest: 10), 60)
        XCTAssertEqual(roundUp(52.376, toNearest: 100), 100)
        XCTAssertEqual(roundUp(52.376, toNearest: 1000), 1000)
    }
}
