//
//  ThreadingTest.swift
//  TwoPods
//
//  Created by Adriaan on 06/06/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//
import XCTest

infix operator ~> {}

@testable import TwoPods

class ThreadingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testThread() {
        
        let expectation = expectationWithDescription("BackgroundTaskExecuted")
        let expectationCallback = expectationWithDescription("CallbackExecuted")
        
        let closure = {
            if NSOperationQueue.currentQueue() != NSOperationQueue.mainQueue() {
                expectation.fulfill()
            }
        }
        
        closure ~> {
            if NSOperationQueue.currentQueue() == NSOperationQueue.mainQueue() {
                expectationCallback.fulfill()
            }
        };

        waitForExpectationsWithTimeout(1.0) { error in
            if let _ = error {
                XCTAssert(false)
            }
        }
    }
}

