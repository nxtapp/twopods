//
// Created by Adriaan on 18/05/16.
// Copyright (c) 2016 TwoBulls. All rights reserved.
//

import Foundation

/// Given a value to round and a factor to round to,
/// round the value to the nearest multiple of that factor.
public func round(number: Double, toNearest: Double) -> Double {
    return round(number / toNearest) * toNearest
}

/// Given a value to round and a factor to round to,
/// round the value DOWN to the largest previous multiple
/// of that factor.
public func roundDown(number: Double, toNearest: Double) -> Double {
    return floor(number / toNearest) * toNearest
}

/// Given a value to round and a factor to round to,
/// round the value DOWN to the largest previous multiple
/// of that factor.
public func roundUp(number: Double, toNearest: Double) -> Double {
    return ceil(number / toNearest) * toNearest
}