//
//  NumberFormatting.swift
//  TwoPods
//
//  Created by Adriaan on 02/05/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import UIKit

public extension Int {
    /// Format an Int with minimal `f` digits
    /// e.g. i = 4, f = "3" -> "  4"
    /// e.g. i = 4, f = "03" -> "004"
    public func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

public extension Float {
    /// Format a Float with `f` decimals
    /// e.g. float = 3.14, f = ".1" -> "3.1"
    /// e.g. float = 3.14, f = ".3" -> "3.140"
    public func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

public extension CGFloat {
    /// Format a CGFloat with `f` decimals
    /// e.g. cgfloat = 3.14, f = ".1" -> "3.1"
    /// e.g. cgfloat = 3.14, f = ".3" -> "3.140"
    public func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

public extension Double {
    /// Format a double with `f` decimals
    /// e.g. double = 3.14, f = ".1" -> "3.1"
    /// e.g. double = 3.14, f = ".3" -> "3.140"
    public func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
