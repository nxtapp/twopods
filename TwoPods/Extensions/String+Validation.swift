//
//  String+Validation.swift
//  TwoPods
//
//  Created by Carsten Witzke on 29/04/16.
//  Copyright © 2016 TwoBulls. All rights reserved.
//

import Foundation

public extension String {
    
    public var isEmail: Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$", options: .CaseInsensitive)
        let match = regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count))
        return match != nil
    }
}