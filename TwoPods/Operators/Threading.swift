//
// Created by Adriaan on 25/05/16.
// Copyright (c) 2016 TwoBulls. All rights reserved.
//

import Foundation

infix operator ~> {}

/// Serial dispatch queue used by the ~> operator
private let queue = dispatch_queue_create("serial-worker", DISPATCH_QUEUE_SERIAL)

/// Executes the lefthand closure on a background thread/queue and,
/// upon completion, the righthand closure on the mainthread.
public func ~>(
        backgroundTask: () -> (),
        callback: () -> ()) {
    dispatch_async(queue) {
        backgroundTask()
        dispatch_async(dispatch_get_main_queue(), callback)
    }
}


/// Executes the lefthand closure on a background thread and,
/// upon completion, the righthand closure on the mainthread.
/// Passes the background closure's output to the main thread closure
public func ~><R>(
        backgroundTask: () -> R,
        callback: (result:R) -> ()) {
    dispatch_async(queue) {
        let result = backgroundTask()
        dispatch_async(dispatch_get_main_queue(), {
            callback(result: result)
        })
    }
}